# Station: A Hidden Roles Game

Station is a hidden roles game by Lily V., licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.][1] This folder contains all material you need to play.

In Station, each player is a survivor of a blast that has greatly damaged a space station; oxygen is limited and quickly depleting, and the survivors have to work together to reactivate and launch the escape pod system before they pass out. However, one of them is secretly the bomber, who is aiming to set another charge to take out any witnesses. The innocent civilians will have to figure out who is the traitor in their midst, and make sure to leave them on the station so they can survive the Lifeboat trip.

Station requires playing with a deck of special cards and can accomodate 4 to 9 players. You can print these cards from the source files at the Station website, or upload them to [Tabletop Simulator][2] or another collaborative game-board app. Unlike some hidden roles games, Station does not require a moderator; the person who deals the cards can also themself play.

## Setting Up the Game

To start the game, seat the players around the table in some turn order, and pick a player to be the dealer. Then, decide what roles will be in the game. The following are recommendations:

* four players: **Civilian**, **Civilian**, **Security Officer** or **Detective**, **Bomber**. (The Bomber role is required for all games.)
* five players: Add a **Mastermind**.
* six players: Add one role among the **Hacker**, the **Detective** or the **Security Officer** (whichever isn’t already in the roles deck).
* seven players: Add the **Muscle**.
* eight players: Add the **Synth**.
* nine players: Use all roles.

Make sure the Bomber role is in the deck, so that it will be distributed to one player. This is mandatory.

Once you’ve set up and shuffled the cards, the dealer deals to each player:

- 1 Role card from the Roles deck (black back; black or red front). 
- 10 cards from the Station deck (white back and front).

Then, the player next clockwise from the dealer starts with the first turn.

## Playing the Game

The Station’s captain perished in the explosion, but the Station computer will accept any of the survivors as acting Captain. Players will have to use their actions to elect a Captain; a Captain then has to prepare the Lifeboat, picking which people are going to be allowed on the Lifeboat and who will be left on the Station, and then launch it, ending the game. While players can communicate in any way they desire at any time, only the player whose turn it is can take action.

There are two parts to a player’s turn:

 - **Act**. The player picks an action from the list below to perform, or passes. Before and after this action, they can also use text that says ‘During your turn’ from the cards in their hand.
- **Breathe**. Once the player has finished taking action (or passing), **they must discard a card from their hand**. A player with no cards in hand passes out and becomes incapacitated for the rest of the game, skipping their turns.

Then the game continues to the next player, until either the Lifeboat launches or all players are incapacitated.

When it’s time to act, players can:

- **Pass.** You can always pass the turn without doing anything.

- **Nominate Someone as Captain.** When a player nominates someone as Captain, other players can challenge them with nominations of their own; players then vote. (Players can nominate themselves; a player that is incapacitated or that was elected as Captain this game cannot vote or be nominated, and if there’s a current Deputy, they also cannot vote, though they can be nominated.) The nominated player with the most votes becomes Captain; ties go to the winner that would go next earliest in turn order (the player whose turn it is wins those ties). A Captain also picks any one player to be their Deputy.

- **Attack Someone Else.** This may cause them to become incapacitated; see below.

- Once elected, Captains can **Prime the Lifeboat**, **Launch** or **Switch Deputies**, and Deputies can **Detain** another player — see below.

At any time before or after acting during the ‘Act’ part of your turn, if you have a card that says ‘During your turn…’, you can use that effect. Follow the instructions on the card: white cards will tell you to discard them to achieve the effect, and thus can only be used once, but this text may also be on the role card, in which case you may use it any time it’s relevant. (When you use any text on your role card, put it on the table visible to everybody — it’s no longer in your hand.)

Once you use an action or pass, and are done using card effects, move on to breathing and the next player has their turn.

## Captains and Deputies

You become a Captain by nominating yourself or challenging someone who is, and being elected. Once elected, the systems of the Station will defer to you.

When a player becomes Captain, they must immediately pick another player to be their Deputy. The Station will give that player additional emergency powers as well. A Deputy remains the Deputy as long as their Captain is in charge and doesn’t switch deputies (with the action below), and any player can be chosen to be Deputy (even multiple times in the same game).

Captains can act on their turn to do one of these things:

- **Switch Deputies.** During their turn, the Captain can pick a different player to be their Deputy.

- **Prime the Lifeboat.** The Captain communicates which players are allowed on board to the Station, and the engine starts heating up. Captains can do this any number of times, picking a different group of passengers each time if they desire, but _must_ do this prior to launching the Lifeboat. The Lifeboat is, for most players, the only way out the Station, so the Captain has full freedom to decide who leaves and who gets stranded.

- **Launch.** Automated systems will round up the passengers (except for players that are at the mercy of another — see below) and launch the Lifeboat. Everyone reveals their role; consequences are counted; and winners and losers are the decided; the game ends.

These actions consume the ‘Act’ part of a Captain’s turn, though they can use the effects of cards as noted above.

Deputies can act on their turn to **Detain** someone. A person who is detained becomes incapacitated and at the Deputy’s mercy (see below for incapacitation). 

Captains can be voted out of their job. During their turn, a player may nominate any player to replace them, in which case you have an election again. Once a player becomes the Captain, they cannot vote in any further elections; and once they’re deposed, they cannot be nominated again to be Captain.

## Incapacitation & Attacks

A character may become **incapacitated** during the game. An incapacitated character is generally unable to act:

- They skip the ‘Act’ portion of their turn. (They still discard a card to breathe.)
- They cannot use the effects of the cards in their hand.
- They cannot vote during elections, and cannot be nominated during elections.
- They cannot hold anyone else at their mercy (below).

You become incapacitated by:

- Discarding the last card in your hand. You become incapacitated for the rest of the game from that point on; or:
- Being attacked or incapacitated using the effect of an action or a card by another player. In this case, you are **at their mercy** until you’re no longer incapacitated.

Once you are at a player’s mercy, they decide your fate: when it’s time for the Lifeboat to leave, you can decide to leave that person on the Station or forcibly bring them in with you on the Lifeboat as prisoners instead.

Characters may be incapacitated, but their players are still able to talk, try to sway the game, and generally communicate in any way they see fit. They just cannot perform the actions indicated above.

Some actions have a chance of letting you incapacitate another player:

- Deputies have an action called **Detain**.

To detain, the Deputy uses the security systems of the Station to immediately incapacitate another player until the Deputy’s next turn. They are at the Deputy’s mercy. If the Deputy loses their authority (because their Captain switches Deputies or is deposed), the detained player is also immediately released and is no longer incapacitated.

- You can **attack** another player.

To attack, players choose a card face down from their hand, then pick two more cards at random to add to that face down, and reveal all these cards at the same time. The player with most dots on the top right corner of the cards they reveal wins the attack. (Some cards have two sets of dots labeled ‘When Attacking’ or ‘When Defending’ — use the relevant column for those.) Ties go to the attacker. The loser discards one of the cards they revealed and becomes incapacitated and at the other player’s mercy until their next turn.

Instead of choosing a white card, a player may pick their own role card from their hand or from the table. (Some role cards have dots like white cards do.) It is also possible that the role card is among the cards in their hand that are chosen randomly, if it wasn’t on the table. If the role card ends up revealed this way, it won’t be discarded; put it on the table for all to see instead.

- The **Security Officer** role card has an effect that lets them, once per game, incapacitate another player until their next turn. This works like the Deputy’s power. (You will have to put your role card on the table if you want to use this effect.)

- Some cards, like the **Stun Baton** or the **Forcefield Generator**, can cancel an attack. They must be used before cards are picked to reveal and discard; follow their instructions to know what to do instead of the attack.

You can only hold a player at your mercy while you yourself aren’t incapacitated. If a player becomes incapacitated, any players at their mercy are no longer incapacitated; likewise, if a player stops being incapacitated, they are no longer at anyone’s mercy. In addition to this, a player can ever only hold one other player at their mercy — if they incapacitate another, the original player stops being incapacitated.

## Ending the Game

The game can end in one of two ways:

- The Captain launches the Lifeboat; or:
 - The last player with any cards in hand discards the last card in their hand. After this happens, the current Captain has one last extra turn (and stops being incapacitated for that turn, if they were); then the game is over per below. (If the Captain doesn’t launch during this last turn, then all players are stranded on the Station as the endgame occurs.)

When the game ends, figure out who was on the Lifeboat and who was on the Station. Players that the Captain have allowed go on the Lifeboat, and the rest are stranded, with one exception: if someone is at another player’s mercy, and the latter player is allowed on the Lifeboat, then that player chooses the incapacitated player’s fate: Lifeboat or stranded on the Station. (The Captain can’t stop you from bringing the other player aboard if they have allowed you to enter the Lifeboat.)

Then, the following occur, in order:

- Everyone who hasn’t yet puts their role card on the table, visible to all.

- The **Mastermind** has a secret life support system and a way out of the Station. The life support system will end them being incapacitated, if they were, and they will use their hidden personal vessel to escape safely. They’re no longer on the Station.

- If a **Synth** is on the Station and not incapacitated, they will use the communications array to transfer themself off-Station. They will survive, even if their body will not.

- If the **Muscle** is aboard the Lifeboat, and the **Mastermind** is too, and neither is incapacitated, they will commandeer the shuttle. If both survive, they will doom every Civilian or ally on the Lifeboat — none of them on the vessel will survive. (If either is incapacitated, or if only one escapes on the Lifeboat, they will be identified by the witnesses and arrested, and lose the game.)

- The hidden charge the remotely-controlled clone-body of the **Bomber** was carrying goes off. If it goes off on the Station, it will utterly destroy it along with anyone else on board that hasn’t escaped somehow; if it goes off on the Lifeboat, it will destroy anyone else on board.

- The Station loses oxygen. The bodies stranded on the Station are doomed.

Civilians and their allies win if _anyone_ on their team makes it out of the Station  alive to serve as a witness. Each of the villains wins separately, but they only win if they manage to destroy all potential witnesses. The Bomber has a remotely-controlled body and wins even if that body is destroyed, but the Muscle and the Mastermind must survive for them to be considered winners.

## Roles & Effects

The following are notes on the roles and on some effects written on the white cards in the game:

### The Civilian

The Civilian has no special effects. A Civilian wins the game if themselves, or any of their allies, escapes on the Lifeboat and survives.

### The Hacker

The Hacker is an ally. They win the game like the Civilian, but they also have an effect that lets them, once per game, take any card that is in the process of being discarded. This can be a card that is discarded during the ‘Breathe’ part of a turn, or because its effect was used, or because its player was attacked and lost. The card will go to your hand rather than being discarded away.

When you use any text from your Role, you must put it on the table, visible to all. Do so as you take that card.

### A Synth

A Synth is a synthetic intelligence who is a citizen on the Station. They are an ally of the civilian and can win like they do, but also they have an additional way to survive — their body is disposable as long as they can use the Station’s remaining functional communication array to send a backup of their consciousness off-system to be a witness and incriminate the villains.

To send their consciousness out, a Synth must be in the Station and not incapacitated at the end of the game. If they do, they will survive even if their body is destroyed.

The Lifeboat doesn’t have enough communication range to send a backup out. If a Synth is on a Lifeboat that explodes or is commandeered, they will not survive with the knowledge they need to incriminate the villains.

### A Detective

You’ve followed the villains to this Station, but couldn’t stop their plot. You have one last chance to find them and bring them to justice.

The Detective is an ally and wins like the Civilians do. In addition to that, they have two effects on their role card they can use:

- Once per game, during the ‘Act’ part of their turn, they look at another player’s hand. They’re the only player to look.

- If a player is at their mercy, they can also look at their hand. Unlike their other effect, this applies as long as the other player is at their mercy — they can look multiple times if they forget, for example.

When you use either effect, put your role card face up on the table.

### A Security Officer

You are an ally and win with the Civilian team. In addition to this, you have the ability, once per game, to make another player incapacitated (and at your mercy) until your next turn begins. When you use that effect, put your role card on the table.

Note that you can only ever hold one character at your mercy. If you’re using the power of the Deputy or win an attack against someone while you have incapacitated someone else using your power, the player that was at your mercy will stop being incapacitated.

### The Bomber

You’ve been hired to do one job: destroy the Station, along with all witnesses. You’ve doomed the Station, but the witnesses remain; you have reactivated your backup remote-control body, with the second hidden charge, to mingle with them and take care of them.

The objective of the Bomber is to position themselves in such a way that all remaining witnesses are destroyed when the second charge goes off at the end of the game. The easiest way to do this is sneak on the Lifeboat, but you also win if your fellow villains manage to commandeer it, even if you get stranded on the Station.

The Bomber is the only villain role that doesn’t need to survive with their body intact to win the game. You’re controlling your body with illegal and rare faster-than-light remote control technology from another sector, and will survive the explosion no matter what.

### The Mastermind

You’ve set this plan in motion, and now you’re making sure it goes off without nary a witness to be found. You’ve come to the Station in a secret shuttle, and your biotech body has a built-in revival system you can use to fake your demise and still escape.

The Mastermind can be stranded on the Station, or even incapacitated, at the end of the game, and still win if all other potential witnesses die. They can do so by maneuvering so that they convince other people they’re the Bomber and allow the real Bomber to sneak on the Lifeboat; or by commandeering the Lifeboat along with hired Muscle (if this role is in the game), so that, even as the Bomber destroys an empty Station, they can still doom everyone aboard (see below).

### The Muscle

You’ve been hired by a Mastermind you’ve never seen to make sure the plan goes off without a hitch. But this is dangerous work, and to win you must survive yourself.

The Muscle wins the game if all witnesses are destroyed and they survive. They don’t have a secret way out — their only way to survive is aboard the Lifeboat. But to win, they must commandeer it: figure out who the Mastermind is, and take the ship together. A Muscle alone on the Lifeboat has no hope to overpower the mob of other passengers, and will be apprehended on arrival.

So: to win, you must be on the Lifeboat with the Mastermind, and neither of you must be incapacitated by a Civilian or ally. (You also want to make sure you don’t get caught in the Bomber’s explosion.) You can fake your way onto the ship — if you have the Mastermind at your mercy, or they do, you’ll free each other and commandeer the ship anyway. But if either is at the mercy of someone else, you’ll have been successfully taken prisoner for the trip.

### Card Notes:

- **Oxygen** and **Debris** do nothing, though you can use Debris as an improvised weapon (hence the dot). If you run out of Oxygen cards, you can still breathe, but time is rapidly running out (as everything you do will cause you to lose track of more and more cards that can actually do something).

- Firearms are more effective without the element of surprise. Use the ‘When Attacking’ or ‘When Defending’ dots on those cards (**Pistol** and **Rifle**) as applicable; they are more useful when you’re on the offensive.

- **Stimulants** will immediately let a player come back to their senses if they were incapacitated. If they were at someone else’s mercy, that also immediately stops.

- When releasing the oxygen in the **Oxygen Tank**, players essentially gain an extra turn — they won’t need to discard cards during their ‘Breathe’ phase until it’s your turn again.

- The **Command Codes** automatically make you Captain and unable to be deposed. There is no election; you immediately get elected. Since there’s a new Captain, the Deputy will also stop being the Deputy, and you will pick a new one (though you may still choose to pick the same person again if you desire). While the Codes make you unable to be _deposed_, another player who acquires command codes (by having them or by using the power of the Hacker to steal them from you) can still use them to become Captain in your stead.

- The **Forecefield Generator** and the **Stun Baton** cancel a possible attack in progress. These will only work against attacks: they cannot stop the Deputy or the Security Officer’s effects. The Stun Baton also allows an attacked player to turn the tables and incapacitate their attacker. If multiple players want to use these cards on the same attack, they all reveal them, then go around the table starting from the player whose turn it is and apply those effects in that order. (Note that you can still protect yourself and others from an attack using the Forcefield Generator, but the incapacitation effect of the Stun Baton is not a direct consequence of the attack and so isn’t canceled that way — you will still end up incapacitated.)

---- 

<p><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Station: A Hidden Roles Game</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://twitter.com/millenomi" property="cc:attributionName" rel="cc:attributionURL">Lily Vulcano</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.</p>

[1]:	http://creativecommons.org/licenses/by-nc-sa/4.0/
[2]:	https://www.tabletopsimulator.com